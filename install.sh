#!/bin/sh
# Before launching this script check if your user have the right
# to use sudo

################################################################################
# Root command
################################################################################
echo "Root command"

# Create sources.list
echo "sources.list"
sudo rm /etc/apt/sources.list
sudo wget -P /etc/apt https://gitlab.com/JaquierM/config-file/raw/master/sys/apt/sources.list

# Create preferences
echo "preferences"
sudo rm /etc/apt/preferences
sudo wget -P /etc/apt https://gitlab.com/JaquierM/config-file/raw/master/sys/apt/preferences

# Create sources.list.d
echo "atom"
curl -sL https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo wget -P /etc/apt/sources.list.d https://gitlab.com/JaquierM/config-file/raw/master/sys/apt/atom.list

echo "gopass"
wget -q -O- https://api.bintray.com/orgs/gopasspw/keys/gpg/public.key | sudo apt-key add -
sudo wget -P /etc/apt/sources.list.d https://gitlab.com/JaquierM/config-file/raw/master/sys/apt/gopass.list

# Installation soft
echo "update, upgrade and install"
sudo apt update
sudo apt upgrade
sudo apt install firefox atom texlive terminator git gopass wireguard uncrustify
sudo apt install yubikey-personalization pdf-presenter-console texmaker

# Create bashrc
sudo rm /root/.bashrc
sudo wget -P /root https://gitlab.com/JaquierM/config-file/raw/master/root/.bashrc

################################################################################
# User command
################################################################################
echo "User command"

# gpg-agent.conf
echo "gpg-agent.conf"
mkdir -p .gnupg
wget -P $HOME/.gnupg https://gitlab.com/JaquierM/config-file/raw/master/usr/gpg-agent.conf

# Installation atom packages
apm install atom-beautify atom-ide-debugger-native-gdb atom-ide-ui build-makefile
apm install gpp-compiler atom-latex language-markdown pdf-view markdown-writer language-rust

# Create bashrc
rm .bashrc
wget -P $HOME https://gitlab.com/JaquierM/config-file/raw/master/usr/.bashrc

# clone password
git clone git@domingues.dyndns-at-home.com:maelPassStore.git $HOME/.password-store
